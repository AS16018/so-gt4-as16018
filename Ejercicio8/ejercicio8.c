#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *pedirTexto();
void contarVocales(char * , int[]);
void imprimir(int []);

void main() {
  char *texto;
  int num[5];
  texto = pedirTexto();
  contarVocales(texto, num);
  imprimir(num);
}

char *pedirTexto(){
  char *cadena;
  cadena = (char *)malloc(100);
  printf("Ingrese el texto en del cual desea encontrar vocales\n");
  fgets(cadena,100,stdin);
  return cadena;
}

void contarVocales(char *ptext, int vocal[]){
  int pos=0, a=1,e=1,i=1,o=1,u=1;
  //inicializamos el vector de las repeticiones con ceros
  for (int j = 0; j < 5; j++) {
    vocal[j]=0;
  }
  while (ptext[pos] != 0) {
    switch (ptext[pos]) {
      case 'a':
      case 'A':
        vocal[0] = a++;
        break;
      case 'e':
      case 'E':
        vocal[1] = e++;
        break;
      case 'i':
      case 'I':
        vocal[2] = i++;
        break;
      case 'o':
      case 'O':
        vocal[3] = o++;
        break;
      case 'u':
      case 'U':
        vocal[4] = u++;
        break;
    }
    pos++;
  }
}

void imprimir(int num[]){
  printf("Las veces que se repite cada vocal son:\n");
  printf("A : %i veces\n", num[0]);
  printf("E : %i veces\n", num[1]);
  printf("I : %i veces\n", num[2]);
  printf("O : %i veces\n", num[3]);
  printf("U : %d veces\n", num[4]);
}
