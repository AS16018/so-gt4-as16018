#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//definimos un struct de empleados
typedef struct{
    char nombre[40];
    int dui;
    float sueldo;
}empleado;

void ordenarNombre(empleado empl[]);
void ordenarSueldo(empleado empl[]);
void mostrarEmpleados(empleado empl[]);

int main() {
  int opcion=1;
  int i;
  empleado empleados[1000];

  printf("\nIngrese registros para un empleado:\n\n");
  while (opcion ==1) {
    //se crean los registros para empleados, segun los que quiera el usuario
    for (i = 0; i < 1000; i++) {
      printf("\nIngrese el nombre del empleado #%i:\n",i+1);
      scanf("%s",&empleados[i].nombre);
      printf("Ingrese el número de Dui:\n");
      scanf("%i",&empleados[i].dui);
      printf("Ingrese el sueldo:\n");
      scanf("%f",&empleados[i].sueldo);

      printf("Desea agregar otro registro?\n1-SI\nOtro número para NO\n");
      scanf("%d",&opcion);
      if (opcion != 1) {
        printf("\n*------------------------------------------------------------------*\n");
        printf("Los registros ordenados por nombre son\n\n");
        ordenarNombre(empleados);
        printf("Los registros ordenados por sueldo son\n\n");
        ordenarSueldo(empleados);
        break;
      }else{
        printf("\nIngrese un Nuevo registo para el empleado #%i\n",i+2);
      }
    }

  }

  return 0;
}
/*
metodo para ordenar alfabeticamente los registro por nombre, comparando los campos
de nombre y reordenandolos atravez del metodo burbuja
*/

void ordenarNombre(empleado empl[]){
empleado cambio[1];
  for (int j = 999; j >0 ; j--) {
    for (int k = 0; k < j; k++) {
      if (empl[k].nombre[0] != '\0') {
        if (empl[k+1].nombre[0] != '\0') {
          if ((strcmp(empl[k].nombre, empl[k+1].nombre)>0)) {
            cambio[0] = empl[k];
            empl[k] = empl[k+1];
            empl[k+1] = cambio[0];
          }
        }

      }
    }
  }
  mostrarEmpleados(empl);
}

/*
metodo para ordenar de mayor a menor los registro por sueldo, comparando los campos
de sueldo y reordenandolos atravez del metodo burbuja
*/
void ordenarSueldo(empleado empl[]){
  empleado cambio[1];
  for (int j = 999; j >0 ; j--) {
    for (int k = 0; k < j; k++) {
      if (empl[k].nombre[0] != '\0') {
        if (empl[k].sueldo < empl[k+1].sueldo) {
          cambio[0] = empl[k];
          empl[k] = empl[k+1];
          empl[k+1] = cambio[0];
        }
      }
    }
  }
  mostrarEmpleados(empl);
}

/*
metodo para mostrar los registro en pantalla
*/
void mostrarEmpleados(empleado empl[]){
  float promedio= 0.0;
  int cantidad = 0;
  for (int j = 0; j < 1000; j++) {
    if (empl[j].nombre[0] != '\0') {
      printf("\nEmpleado #%i\n", j+1);
      printf("NOMBRE : %s\nDUI : %i\nSUELDO : %.2f\n",empl[j].nombre,empl[j].dui,empl[j].sueldo);
      promedio += empl[j].sueldo;
      cantidad++;
    }else{
      printf("\nEl promedio de los sueldos es : %0.2f\n", promedio/cantidad );
      printf("\n*------------------------------------------------------------------*\n");
      break;
    }
  }

}
