#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

float solucionPolinomio(float coefi[], float x, int cantidad);
void mostrarPolinomio(float coefi[], int cantidad);

int main() {

  int grado=0, i;
  float punto, coeficiente;
  float *coeficientes;

  while (grado<=0) {
    printf("Ingrese el grado del polinomio:\n");
    scanf("%d", &grado);
    if (grado<=0) {
      printf("\nEl grado del polinomio tiene que ser mayor a 0\n");
    }
  }

  coeficientes = (float *)malloc((grado+1)*sizeof(float));

  printf("\nIngrese los coeficientes para el polinomio de grado %i\n\n",grado);
  for (i = 0; i < grado+1; i++) {
    printf("coeficiente #%i : ",i+1);
    scanf("%f",&coeficiente);
    coeficientes[i] = coeficiente;
  }

  printf("\nIngrese el punto (x) a evaluar en el polinomio:\n");
  scanf("%f",&punto);

  mostrarPolinomio(coeficientes,grado);
  printf("\nP(%.2f)= %.2f\n", punto, solucionPolinomio(coeficientes, punto, grado));



  return 0;
}
/*
metodo que realiza la operacion matematica para resolver el polinomio de n grado
*/
float solucionPolinomio(float coefi[], float x, int cantidad){
  float resul = coefi[0];
  for (int i = 1; i < cantidad+1; i++) {
      resul = resul + (coefi[i]*(pow(x, i)));
  }
  return resul;
}

void mostrarPolinomio(float coefi[], int cantidad) {

  //char donde se almacenaran los float convertidos a caracter
  char *float_a_char = (char *)malloc(10*sizeof(char));
  //char que almacenara las cadenas concatenadas
  char *polinomio = (char *)malloc(1000*sizeof(char));
  /*
  copiamos la primer cadena a la variable donde se concatenaran los string para formar el polinomio expresado
  */
  strcpy(polinomio, "P(x) = ");

  for (int i = cantidad; i>0; i--) {
    /*
    Haciendo uso de gcvt iremos convirtiendo los flotantes a string como los coeficientes
    y exponentes para formar el polinomio expresado como una cadena
    */
    gcvt(coefi[i], 3, float_a_char);
    //concatenamos cada conversión a la cadena principal con strcat
    strcat(polinomio, float_a_char);
    strcat(polinomio, "x");
    //si el exponente es 1, no es necesario imprimirlo
    if (i != 1) {
      strcat(polinomio, "^");
      gcvt(i, 2, float_a_char);
      strcat(polinomio, float_a_char);
    }
    //se va agregando + a la cadena
    strcat(polinomio, " + ");
  }
  //agregamos el ultimo valor de la cadena donde el exponente es 0
  gcvt(coefi[0], 3, float_a_char);
  strcat(polinomio, float_a_char);
  printf("\nEl resultado para el polinomio %s es:\n", polinomio);
  free(polinomio);
}
