#include <stdio.h>
#include <stdlib.h>

float maximo (float datos[], int n);
float minimo (float datos[], int n);
float media(float datos[], int n);

int main() {
  int cantidad, i;
  float *p_datos;
  float dato;


  printf("Ingrese el numero de datos a evaluar:\n");
  scanf("%d",&cantidad);
  //apuntamos hacia el bloque de memoria reservada por malloc con un tamaño de
  //20 veces lo que mide un float
  p_datos = (float *)malloc(cantidad*sizeof(float));

  printf("Ingrese los valores:\n");
  for (i = 0; i < cantidad; i++) {
    printf("El valor para el dato #%d : ",i+1);
    scanf("%f",&dato);
    p_datos[i]=dato;
  }
  //Imprimimos los datos del vector
  printf("\nLos datos son: \n");
  for (i = 0; i < cantidad; i++) {
    printf("%.2f\t", p_datos[i]);
  }
  printf("\nEl máximo valor es : %.2f\n", maximo(p_datos, cantidad));
  printf("El mínimo valor es : %.2f\n", minimo(p_datos, cantidad));
  printf("La media de los valores es : %.2f\n", media(p_datos, cantidad));

  return 0;
}

float maximo (float datos[], int n){
  int i;
  float maximo = datos[0];
  for (i = 0; i < n; i++) {
    if (datos[i] > maximo) {
      maximo = datos[i];
    }
  }
  return maximo;
}

float minimo (float datos[], int n){
  int i;
  float minimo = datos[0];
  for (i = 0; i < n; i++) {
    if (datos[i] < minimo) {
      minimo = datos[i];
    }
  }
  return minimo;
}

float media(float datos[], int n){
  int i;
  float suma = 0.0;
  for (i = 0; i < n; i++) {
    suma += datos[i];
  }
  suma = suma/n;
  return suma;
}
