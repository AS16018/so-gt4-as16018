#include <stdio.h>
#include <string.h>

//firma de metodo que recibe como parametros tres punteros de tipo char
void ordenar(char *pal1, char *pal2, char *pal3);

int main() {

  char palabra1[50];
  char palabra2[50];
  char palabra3[50];

//Pedimos por consola las palabras al usuario
  printf("Ingrese tres palabras para ordenarlas alfabeticamente de manera descendente\n");
  printf("Ingrese la primera palabra :\n");
  //Se hace uso de fgets ya que guarda la palabra completa en caso de llevar espacios
  fgets(palabra1,50,stdin);
  printf("Ingrese la segunda palabra :\n");
  fgets(palabra2,50,stdin);
  printf("Ingrese la tercera palabra :\n");
  fgets(palabra3,50,stdin);
  //se llama al metodo que ordena las palabras
  ordenar(palabra1, palabra2, palabra3);

  return 0;
}
//metodo para mostrar el orden en el que se puedan dar las palabras
void ordenar(char *pal1, char *pal2, char *pal3){
  //se hace uso del metodo strcmp() para comparar el valor tipo char hacia donde apuntan los punteros
  if (strcmp(pal1, pal2)<=0 && strcmp(pal1, pal3)<=0) {
    if (strcmp(pal2, pal3)<=0) {
      printf("El orden en forma alfabetica y desendente es:\n[1] %s[2] %s[3] %s", pal1,pal2,pal3);
    }else{
      printf("El orden en forma alfabetica y desendente es:\n[1] %s[2] %s[3] %s", pal1,pal3,pal2);
    }
  }else if (strcmp(pal2, pal3)<=0 && strcmp(pal2, pal1)<=0) {
    if (strcmp(pal1, pal3)<=0) {
      printf("El orden en forma alfabetica y desendente es:\n[1] %s[2] %s[3] %s", pal2,pal1,pal3);
    }
    else{
      printf("El orden en forma alfabetica y desendente es:\n[1] %s[2] %s[3] %s", pal2,pal3,pal1);
    }
  }else if (strcmp(pal3, pal2)<=0 && strcmp(pal3, pal1)<=0) {
    if (strcmp(pal1, pal2)<=0) {
      printf("El orden en forma alfabetica y desendente es:\n[1] %s[2] %s[3] %s", pal3,pal1,pal2);
    }else{
      printf("El orden en forma alfabetica y desendente es:\n[1] %s[2] %s[3] %s", pal3,pal2,pal1);
    }
  }
}
